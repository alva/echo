import Cocoa

class AppDelegate: NSObject, NSApplicationDelegate {
    func applicationDidFinishLaunching(_ notification: Notification) {
        FileHandle.standardInput.readabilityHandler = {
            FileHandle.standardOutput.write($0.availableData)
        }
    }

    func applicationWillTerminate(_ notification: Notification) {
    }
}

NSApplication.shared.delegate = AppDelegate()
exit(NSApplicationMain(CommandLine.argc, CommandLine.unsafeArgv))
